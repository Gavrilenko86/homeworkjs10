// знаходимо всі елементи вкладок
const tabs = document.querySelectorAll(".tabs-title");

// знаходимо всі елементи контенту вкладок
const contents = document.querySelectorAll(".tabs-content li");

// додаємо подію "click" до кожної вкладки
tabs.forEach((tab, index) => {
  tab.addEventListener("click", () => {
    // приховуємо весь контент
    contents.forEach((content) => {
      content.style.display = "none";
    });

    // відображаємо відповідний контент
    contents[index].style.display = "block";

    // відмічаємо активну вкладку
    tabs.forEach((tab) => {
      tab.classList.remove("active");
    });
    tab.classList.add("active");
  });
});
